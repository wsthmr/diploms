﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Diploms.Classes.ViewModels;
using Diploms.Classes.Tables;
using System.Data.SQLite;
using System.IO;
using System.Data;

namespace Diploms
{
    public enum NameTable
    {
        Students,
        Groups,
        Specialitys,
        Atchive,
        Projects
    }

    class Test
    {
        public string Name { get; set; }
    }

    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    /// 

    public partial class MainWindow : Window
    {
        public static DataGrid dataGrid;
        public NameTable state = NameTable.Students;

        
        // Ссылка на соединенеие.
        public static SQLiteConnection connection;

        // Сущность для передачи запросов и получения результатов.
        private SQLiteCommand cmd;

        // Название базы.
        private string dataBaseName;

        public MainWindow()
        {
            InitializeComponent();
            dataGrid = grid;

            connection = new SQLiteConnection();
            cmd = new SQLiteCommand();

            // Название рандомное.
            dataBaseName = "dbDiploms.sqlite";
        }

        private void Students_Click(object sender, RoutedEventArgs e)
        {
            //grid.ItemsSource = (new StudentViewModel()).Students;
        }

        private void Groups_Click(object sender, RoutedEventArgs e)
        {
           // grid.ItemsSource = (new GroupsViewModel()).Groups;
        }

        private void Specialitys_Click(object sender, RoutedEventArgs e)
        {
           // grid.ItemsSource = (new SpecialityViewModel()).Speciality;
        }

        private void Diploms_Click(object sender, RoutedEventArgs e)
        {
            //grid.ItemsSource = (new DiplomsViewModel()).Diploms;
        }


        // Событие, вызываемое при загрузке формы.
        private void OnLoaded(object sender, RoutedEventArgs e)
        {

            // Проверяем что такая база есть, если нет, создаем.
            if (!File.Exists(dataBaseName))
                SQLiteConnection.CreateFile(dataBaseName);

            try
            {
                // Задаем конфиги для соедниения.
                connection = new SQLiteConnection($"Data Source={dataBaseName};Version=3;");

                // Открываем соединение.
                connection.Open();

                // Указываем соединение для штуик, через которую будем отправлять запросы.
                cmd.Connection = connection;
                grid.ItemsSource = StudentViewModel.Students;
            }
            catch(SQLiteException maslina)
            {
                MessageBox.Show("Maslina poymana, text: " + maslina.Message);
            }

        }
    }
}
