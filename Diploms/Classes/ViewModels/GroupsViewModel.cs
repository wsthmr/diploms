﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Input;
using Diploms.Classes.Tables;
using System.Windows.Forms;

namespace Diploms.Classes.ViewModels
{
    class GroupsViewModel
    {
        public static ObservableCollection<Groups> Groups { get; set; } = new ObservableCollection<Groups>();
        public Student SelectedGroups { get; set; }

        public ICommand AddRowCommand { get; set; }
        public ICommand GetRowInfoCommand { get; set; }

        public GroupsViewModel()
        {
            AddRowCommand = new RelayCommand(AddRow);
            GetRowInfoCommand = new RelayCommand(GetRowInfo);
            Groups.Add(new Groups("P-414"));
        }

        private void AddRow()
        {
             Groups.Add(new Groups("P-414"));
        }

        private void GetRowInfo()
        {
            if (SelectedGroups != null)
                MessageBox.Show($"\nГруппа: {SelectedGroups.name_group}");
        }
    }
}
