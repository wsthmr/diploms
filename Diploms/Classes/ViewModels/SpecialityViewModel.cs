﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Input;
using Diploms.Classes.Tables;
using System.Windows.Forms;

namespace Diploms.Classes.ViewModels
{
    class SpecialityViewModel
    {
        public static ObservableCollection<Speciality> Speciality { get; set; } = new ObservableCollection<Speciality>();
        public Student SelectedSpeciality { get; set; }

        public ICommand AddRowCommand { get; set; }
        public ICommand GetRowInfoCommand { get; set; }

        public SpecialityViewModel()
        {
            AddRowCommand = new RelayCommand(AddRow);
            GetRowInfoCommand = new RelayCommand(GetRowInfo);
            Speciality.Add(new Speciality("Программист"));
        }

        private void AddRow()
        {
            Speciality.Add(new Speciality("Программист"));
        }

        private void GetRowInfo()
        {
            if (SelectedSpeciality != null)
                MessageBox.Show($"\nНазвание Специальности: {SelectedSpeciality.name_speciality}");
        }
    }
}
