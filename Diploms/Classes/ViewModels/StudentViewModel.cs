﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Input; 
using Diploms.Classes.Tables;
using System.Windows.Forms;
using System.Data.SQLite;

namespace Diploms.Classes.ViewModels
{
    public static class StudentViewModel
    {
        private static ObservableCollection<Student> students;
        public static ObservableCollection<Student> Students
        {
            get
            {
                if(students == null) Load();
                return students;
            }
        }
        public static Student SelectedStudent { get; set; }

        public static ICommand AddRowCommand { get; set; }
        public static ICommand GetRowInfoCommand { get; set; }

        private static void Load()
        {

            AddRowCommand = new RelayCommand(AddRow);
            GetRowInfoCommand = new RelayCommand(GetRowInfo);
            

            students = new ObservableCollection<Student>();

            {
                // Тут мы создаем запрос.
                new SQLiteCommand("INSERT INTO Student (id_stud, name, surename, lastname, birth_date, name_group, name_specialty, date_of_issue, date_of_protection, date_of_receipt) VALUES(1, 'S', 'E', 'R', '2/26/2008', '301-P', 'T', '2/26/2008', '2/26/2008', '2/26/2008'); ", MainWindow.connection);

                // Тут мы запускаем 
                using (SQLiteDataReader reader = new SQLiteCommand("SELECT * FROM Student;", MainWindow.connection).ExecuteReader())
                {
                    while (reader.Read())
                    {
                        // Считываем данные в спсиок
                        students.Add(new Student(
                            reader.GetInt32(0), // Считываем строку из первого стобца
                            reader.GetString(1), // Считываем строку из 2-ого стобца
                            reader.GetString(2), // Считываем строку из 3-ого стобца
                            reader.GetString(3), // Считываем строку из 4-ого стобца
                            Convert.ToDateTime(reader.GetString(4), new System.Globalization.CultureInfo("en-US")), // Считываем дату из 5-ого стобца
                            reader.GetString(5), // Считываем строку из 6-ого стобца
                            reader.GetString(6), // Считываем строку из 7-ого стобца
                            Convert.ToDateTime(reader.GetString(7), new System.Globalization.CultureInfo("en-US")), // Считываем дату из 8-ого стобца
                            Convert.ToDateTime(reader.GetString(8), new System.Globalization.CultureInfo("en-US")), // Считываем дату из 9-ого стобца
                            Convert.ToDateTime(reader.GetString(9), new System.Globalization.CultureInfo("en-US"))) // Считываем дату из 10-ого стобца
                            );
                    }
                }

            }
        }

        private static void AddRow()
        {
            int id = Students.Count > 0 ? Students[Students.Count - 1].id_stud + 1 : 0; 
            Students.Add(new Student(id, "Вася", "Пупкин", "Валерьевич", DateTime.Today, "P-414", "Программист", DateTime.Today, DateTime.Today, DateTime.Today));
        }

        private static void GetRowInfo()
        {
            if (SelectedStudent != null)
                MessageBox.Show($"Имя: {SelectedStudent.name}\nФамилия: {SelectedStudent.surename}\nОтчество: {SelectedStudent.lastname}\nДата Рождения: {SelectedStudent.birth_date}\nГруппа: {SelectedStudent.name_group}\nСпециальность: {SelectedStudent.name_speciality}\nДата Поступления: {SelectedStudent.date_of_receipt}\nДата Защиты: {SelectedStudent.date_of_protection}\nДата Выпуска: {SelectedStudent.date_of_issue}");
        }
    }
}
