﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Input;
using Diploms.Classes.Tables;
using System.Windows.Forms;

namespace Diploms.Classes.ViewModels
{
    class DiplomsViewModel
    {
        public static ObservableCollection<Diplom> Diploms { get; set; } = new ObservableCollection<Diplom>();
        public Student SelectedDiploms { get; set; }

        public ICommand AddRowCommand { get; set; }
        public ICommand GetRowInfoCommand { get; set; }

        public DiplomsViewModel()
        {
            AddRowCommand = new RelayCommand(AddRow);
            GetRowInfoCommand = new RelayCommand(GetRowInfo);
            int id = Diploms.Count > 0 ? Diploms[Diploms.Count - 1].id_diplom + 1 : 0;
            Diploms.Add(new Diplom(id, "P-414"));
        }

        private void AddRow()
        {
            int id = Diploms.Count > 0 ? Diploms[Diploms.Count - 1].id_diplom + 1 : 0;
            Diploms.Add(new Diplom(id, "P-414"));
        }

        private void GetRowInfo()
        {
            if (SelectedDiploms != null)
                MessageBox.Show($"\nНазвание Диплома: {SelectedDiploms.name}");
        }
    }
}
