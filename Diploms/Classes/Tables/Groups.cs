﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diploms.Classes.Tables
{
    class Groups
    {
        public string name_group { get; set; }

        public Groups(string _name_group) 
        {
            this.name_group = _name_group;
        }
    }
}
