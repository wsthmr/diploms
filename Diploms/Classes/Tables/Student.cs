﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diploms.Classes.Tables
{
    public class Student
    {
        public int id_stud;
        public string name { get; set; }
        public string surename { get; set; }
        public string lastname { get; set; }
        public DateTime birth_date { get; set; }
        public string name_group { get; set; }
        public string name_speciality { get; set; }
        public DateTime date_of_receipt { get; set; }
        public DateTime date_of_protection { get; set; }
        public DateTime date_of_issue { get; set; }

        public Student(int _id_stud, string _name, string _surname, string _lastname, DateTime _birth_date, string _name_group, string _name_speciality, DateTime _date_of_receipt, DateTime _date_of_protection, DateTime _date_of_issue)
        {
            this.id_stud = _id_stud;
            this.name = _name;
            this.surename = _surname;
            this.lastname = _lastname;
            this.birth_date = _birth_date;
            this.name_group = _name_group;
            this.name_speciality = _name_speciality;
            this.date_of_receipt = _date_of_receipt;
            this.date_of_protection = _date_of_protection;
            this.date_of_issue = _date_of_issue;
        }

        public Student()
        {

        }
    }
}
