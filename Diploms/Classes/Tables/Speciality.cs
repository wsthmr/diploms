﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diploms.Classes.Tables
{
    class Speciality
    {
        public string name_speciality { get; set; }

        public Speciality(string _name_speciality)
        {
            this.name_speciality = _name_speciality;
        }
    }
}
