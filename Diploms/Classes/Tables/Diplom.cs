﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diploms.Classes.Tables
{
    class Diplom
    {
        public int id_diplom;
        public string name { get; set; }

        public Diplom(int _id_diplom, string _name)
        {
            this.id_diplom = _id_diplom;
            this.name = _name;
        }
    }
}
