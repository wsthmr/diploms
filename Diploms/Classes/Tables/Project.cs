﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diploms.Classes.Tables
{
    class Project
    {
        public int id_project { get; set; }
        public int id_stud { get; set; }
        public int id_diplom { get; set; }
        public string name_speciality { get; set; }
        public int mark { get; set; }

        public Project(int _id_project, int _id_stud, int _id_diplom, string _name_speciality, int _mark)
        {
            this.id_project = _id_project;
            this.id_stud = _id_stud;
            this.id_diplom = _id_diplom;
            this.name_speciality = _name_speciality;
            this.mark = _mark;
        }
    }
}
