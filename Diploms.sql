CREATE TABLE Students
(
	id_stud INTEGER PRIMARY KEY AUTOINCREMENT,
	name VARCHAR(64),
	surename VARCHAR(64),
	lastname VARCHAR(64),
	birth_date DATE,
	name_group VARCHAR(10),
	name_speciality VARCHAR(64),
	date_of_receipt DATE,
	date_of_protection DATE,
	date_of_issue DATE
);

CREATE TABLE Groups
(
	name_group VARCHAR(64) PRIMARY KEY
);

CREATE TABLE Speciality
(
	name_speciality VARCHAR(64) PRIMARY KEY
);

CREATE TABLE Diploms
(
	id_diplom INTEGER PRIMARY KEY AUTOINCREMENT,
	name VARCHAR(64)
);

CREATE TABLE Archive
(
	id_record INTEGER PRIMARY KEY AUTOINCREMENT,
	id_project INTEGER PRIMARY KEY AUTOINCREMENT,
	id_stud INTEGER,
	id_diplom INTEGER,
	name_speciality VARCHAR(64),
	mark INTEGER
);

CREATE TABLE Project
(
	id_project INTEGER PRIMARY KEY AUTOINCREMENT,
	id_stud INTEGER,
	id_diplom INTEGER,
	name_speciality VARCHAR(64),
	mark INTEGER
);

INSERT INTO Student (id_stud, name, surename, lastname, birth_date, name_group, name_speciality, date_of_issue, date_of_protection, date_of_receipt)
VALUES (1 , 'S', 'E', 'R', '2/26/2008', '301-P', 'T', '2/26/2008', '2/26/2008', '2/26/2008');